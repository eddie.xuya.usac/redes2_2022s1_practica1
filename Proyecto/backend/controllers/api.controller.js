const BD = require("../config/conn");

exports.getDevs = async (req, res) => {
  try {
    const conn = await BD.SSHConnection.then(conn =>{
        let sql = `SELECT nombre, img_url, carnet, info FROM Devs;`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            res.send(result);
          });        
    });
  } catch (error) {
    console.log("Error al conectar a la base de datos => ", error);
    res.json({});
  }
};

exports.getImgs = async(req, res) => {
  try {
    const conn = await BD.SSHConnection.then(conn =>{
        let sql = `SELECT nombre, img_url FROM Img`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            res.send(result);
          });        
    });
  } catch (error) {
    console.log("Error al conectar a la base de datos => ", error);
    res.json({});
  }
}
