require('dotenv').config()
var mysql = require('mysql2');
var Client = require('ssh2').Client;
var sshClient = new Client();

dbServer = {
    user: process.env.USER_MYSQL,
    port: process.env.PORT_MYSQL,
    password: process.env.PASSWORD_MYSQL,
    host: process.env.HOST_MYSQL,
    database: process.env.DATABASE_MYSQL
}

// EC2 Credentials
const tunnelConfig = {
    host: process.env.HOST_EC2,//'ec2-3-85-166-41.compute-1.amazonaws.com',   // DNS ipv4 de la EC2 no es la ip es un url
    port: process.env.PORT_EC2,// 22 // puerto SSH
    username: process.env.USER_EC2,//'ubuntu',   // usuario en este caso es una instancia de Ubuntu
    privateKey: require('fs').readFileSync(process.env.PATH_PRIVATE_KEY)  //ruta de su llave .pem de la EC2 , antes darle permisos al archivo
}

// Enlazamos las 2 conexiones
const forwardConfig = {
    srcHost: tunnelConfig.host, //process.env_HOST_MYSQL, //'rds-redes2-p1.cox8eu9vovga.us-east-1.rds.amazonaws.com', // punto acceso de RDS
    srcPort: tunnelConfig.port, //process.env.PORT_MYSQL,//3306, // puerto
    dstHost: dbServer.host, // destination database
    dstPort: dbServer.port // destination port
};


// Creamos el promise para la conexion
const SSHConnection = new Promise((resolve, reject) => {
    sshClient.on('ready', () => {
        //Le seteamos las credenciales
        sshClient.forwardOut(
        forwardConfig.srcHost,
        forwardConfig.srcPort,
        forwardConfig.dstHost,
        forwardConfig.dstPort,
        (err, stream) => {
             if (err) reject(err);
            // creamos un servidor de base de datos
            const updatedDbServer = {
                 ...dbServer,
                 stream
            };
            // realizamos conexion a mysql rds
            const connection =  mysql.createConnection(updatedDbServer);
           //  hacemos una validacion que si conecte          
           connection.connect((error) => {
            if (error) {
                console.log('Error de conexion')
                reject(error);
            }
            resolve(connection);
            console.log('Conectado a Base de datos');
            });
            //retornamos la conexion
            return connection;
       });
    }).connect(tunnelConfig);
});

exports.SSHConnection = SSHConnection;