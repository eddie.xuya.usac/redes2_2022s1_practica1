
//Routes
const indexRouter = require('./routes/index.route.js')
const apiRouter = require('./routes/api.route.js')
//Servidor
require('dotenv').config()
var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var app = express();
const cors = require('cors');
var corsOptions = { origin: true, optionsSuccessStatus: 200 };
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.use(morgan('dev'));
app.use("/", indexRouter);
app.use("/api", apiRouter);

var port = process.env.PORT;
app.listen(port, function(){
console.log('Server on port:', port);
});

module.exports = app;


