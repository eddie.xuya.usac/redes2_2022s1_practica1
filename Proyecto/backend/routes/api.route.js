const express = require("express")
const router = express.Router()
const apiController = require("../controllers/api.controller")

router.get("/devs", apiController.getDevs)

router.get("/imgs", apiController.getImgs)
module.exports = router