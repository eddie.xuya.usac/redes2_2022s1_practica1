export const Services = {
    getTabs: () => {
        return [
            {
                id: '1',
                name: 'Home',
                route: '/home'
            },
            {
                id: '2',
                name: 'Administradores',
                route: 'admin'
            },
            {
                id: '3',
                name: 'Desarrolladores',
                route: 'devs'
            }
        ];
    },
    getAdmins: () => {
        return [
        {
            id: '1',
            name: 'John Smith',
            position: 'CEO',
            image: 'https://i.pravatar.cc/600',
            description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries',

        },
        {
            id: '2',
            name: 'Jane Doe',
            position: 'Accounting',
            image: 'https://i.pravatar.cc/300',
            description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries',

        },
        {
            id: '3',
            name: 'Mike Fell',
            position: 'Sales',
            image: 'https://i.pravatar.cc/700',
            description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries',

        }
    ]
},
    getDevs: () => {
        return [
            {
                id: '1',
                name: 'John Smith 1',
                carnet: '20000000',
                image: 'https://www.fellini.com.br/plugins/images/users/4.jpg',    
            },
            {
                id: '2',
                name: 'John Smith 1',
                carnet: '20000000',
                image: 'https://www.fellini.com.br/plugins/images/users/4.jpg',
            },
            {
                id: '3',
                name: 'John Smith 1',
                carnet: '20000000',
                image: 'https://www.fellini.com.br/plugins/images/users/4.jpg'
            }
        ];
    },
    getCarousel: ()=> {
        return [
            {
                id: '1',
                image: '',
                description: ''
            },
            {
                id: '2',
                image: '',
                description: ''
            },
            {
                id: '3',
                image: '',
                description: ''
            }
        ]
    }
}