import { Route, Routes } from "react-router-dom";
import AdminPage from "./pages/admin";
import DevelopersPage from "./pages/developers";
import HomePage from "./pages/home";
import ImagesComponent from "./pages/images";
import LandingPage from "./pages/landing";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<HomePage />}>
          <Route index element={<LandingPage />} />
          <Route path="admin" element={<AdminPage />} />
          <Route path="home" element={<LandingPage />} />
          <Route path="developers" element={<DevelopersPage />} />/
          <Route path="imgs" element={<ImagesComponent />} />/
        </Route>
      </Routes>
    </div>
  );
}

export default App;
