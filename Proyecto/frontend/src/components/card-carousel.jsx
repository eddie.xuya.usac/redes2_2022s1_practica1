import React from 'react'
import ReactCardCarousel from 'react-card-carousel';

const  CARD_STYLE = {
  height: '300px',
  width: '500px',
  paddingTop: '80px',
  textAlign: 'center',
  background: '#FFF',
  color: '#000',
  fontSize: '12px',
  borderRadius: '10px',
  paddingBottom: '10px',
  paddingLeft: '10px',
  paddingRight: '10px'
};

const IMG_STYLE = {
    width: '150px',
    height: '150px',
    borderRadius: '100%',
    border: '5px solid black',
}

export default function CardCarouselComponent(props) {
  return (
    <ReactCardCarousel autoplay={ true } autoplay_speed={ 2500 }>
    {
      props.elements && props.elements.map((element) => (
      <div key={element.id} style={ CARD_STYLE }>
        <img style={IMG_STYLE} src={element.image} alt="" />
        {element.name && <h2>{element.name}</h2>}
        {element.carnet && <p>{element.carnet}</p>}
        {element.position && <h3>{element.position}</h3>}
        {element.description && <p>{element.description}</p>}
        {element.info && <p>{element.info}</p>}
      </div>
      ))
    }
  </ReactCardCarousel>
  )
}
