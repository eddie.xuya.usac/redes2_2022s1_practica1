import React from 'react'
import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css"; 

export default function HeroCarouselComponent(props) {
 

  return (
    <div className='carouselc-ontainer'>
        <Carousel>
        {
          props.elements && props.elements.map((element) => (
            <div key={element.img_url} > 
            <img alt='img' src={element.img_url} />
            <p className="legend">{element.nombre}</p>
        </div>
          ))
        }
        </Carousel>
    </div>
  )
}
