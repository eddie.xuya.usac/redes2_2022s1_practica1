import React, { useEffect, useState } from 'react'
import { env } from '../environtment';
import CardCarouselComponent from '../components/card-carousel'

export default function DevelopersPage() {
  const [developers, setDevelopers] = useState([]);

  useEffect(() => {
    fetch(`${env.API_URL}/devs`)
    .then(res => res.json())
    .then(res => res.map(value => ({
      id: value.carnet,
      name: value.nombre,
      carnet: value.carnet,
      image: value.img_url,    
      info: value.info,
    })))
    .then((devResponse) => {
      setDevelopers(devResponse);
    });
  }, []);
  

  return (
    <div className='container-all'>
      <div className="background-devs"></div>
      <div className="overlay devs-page">
        <h1>Desarrolladores</h1>
        <div className="carousel-container">
          <CardCarouselComponent elements={developers} />
        </div>
      </div>
    </div>
  )
}
