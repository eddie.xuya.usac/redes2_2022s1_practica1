import React from 'react'
import { Link, Outlet } from 'react-router-dom'

export default function HomePage() {
  return (
    <>
    <nav className='navbar'>
        <ul>
          <li>
          <Link to="/home">Inicio</Link>
          </li>
          <li>
          <Link to="/admin">Administradores</Link>
          </li>
          <li>
          <Link to="/developers">Desarrolladores</Link>
          </li>
          <li>
          <Link to="/imgs">Imagenes</Link>
          </li>
        </ul>
      </nav>
      <div>
        <Outlet />
      </div>
    </>
  )
}
