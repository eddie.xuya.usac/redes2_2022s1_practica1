import React, { useEffect, useState } from 'react'
import CardCarouselComponent from '../components/card-carousel'
import { Services } from '../services/data';

export default function AdminPage() {

  const [admins, setAdmins] = useState([]);

  useEffect(() => {
    const admins = Services.getAdmins();
    setAdmins(admins);
  }, []);

  return (
    <div>
      <div className='container-all'>
      <div className="background-admin"></div>
      <div className="overlay admin-page">
        <h1>Administradores</h1>
        <div className="carousel-container">
          <CardCarouselComponent elements={admins} />
        </div>
      </div>
    </div>
    </div>
  )
}
