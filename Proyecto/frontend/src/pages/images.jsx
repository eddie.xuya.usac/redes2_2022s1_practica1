import React, { useEffect, useState } from 'react'
import HeroCarouselComponent from '../components/hero-carousel'
import { env } from '../environtment';

export default function ImagesComponent() {
  const [images, setImages] = useState([]);

  useEffect(() => {
    fetch(`${env.API_URL}/imgs`)
    .then(res => res.json())
    .then((imgResponse) => {
      setImages(imgResponse);
    });
  }, []);
  return (
    <div>
      <div className='container-all'>
      <div className="background-admin"></div>
      <div className="overlay admin-page">
        <h1>Imágenes</h1>
        <div className="carousel-container">
          <HeroCarouselComponent elements={images} />
        </div>
      </div>
    </div>
    </div>
  )
}
