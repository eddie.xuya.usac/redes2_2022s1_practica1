# Redes2_2022s1_Practica1

**UNIVERSIDAD DE SAN CARLOS DE GUATEMALA**<br>
**FACULTAD DE INGENIERÍA**<br>
**ESCUELA DE INGENIERÍA EN CIENCIAS Y SISTEMAS**<br>
**REDES DE COMPUTADORAS 2**<br>
**SECCIÓN N**<br>

<br>
<br>
<p align="center"> 
  <img align="center" width="440px" src="imgs/logo_usac.svg" />
</p>

**Grupo:** 13<br>
**Integrantes:**
| Nombre                             | Carné     |
|------------------------------------|-----------|
| Emely Yecenia García Monge         | 201610649 |
| Eddie Orlando Xuyá Monroy          | 201113930 |
| Juan Antonio Pineda Espino         | 201404405 |

**Catedrático:** Ing. Manuel Lopez<br>
**Auxiliar:** Edgar Daniel Cil<br>
**Fecha:** 19/02/2022<br>

### Notas
- El escenario 1 está completo con port-channel en modo lacp y stp en modo pvst. Solo hay que tomar los tiempos.
- El escenario 3 está con el port channel en modo pagp pero sin stp configurado. 
- El archivo de Practica_Implementacion_13 solo tiene el port-channel configurado en modo lacp

Los tres archivos ya tienen configurado los servidores, intervlan y subnetting( les dejo el link de la pag que utilicé como guía)[https://aprendaredes.com/cgi-bin/ipcalc/ipcalc_cgi1?host=192.168.14.0&mask1=24&mask2=255.255.255.224].
<br>

Quedaría pendiente terminar los esscenarios y hacer la tabla convergencia e implementar seguridad. 

### R0 (Switch Capa 3)
> enable <br>
config t <br>
vtp domain g13 <br>
vtp password g13 <br>
vtp mode server <br>
vlan 14 <br>
name Ventas <br>
exit <br>
vlan 24 <br>
name Distribucion <br>
exit <br>
vlan 34 <br>
name Administracion <br>
exit <br>
vlan 44 <br>
name Servidores <br>
exit <br>
exit <br>
show vtp status <br>
show vl <br>
conf t <br>
interface fa0/1 <br> 
switchport trunk encapsulation dot1q <br>
switchport mode trunk <br>
interface fa0/2 <br>
switchport trunk encapsulation dot1q <br>
switchport mode trunk <br>
exit <br>
ip routing <br>
int vlan 14<br>
description vinterface vlan ventas<br>
ip address 192.168.14.1 255.255.255.224<br>
exit<br>
int vlan 24<br>
description vinterface vlan distribucion<br>
ip address 192.168.14.33 255.255.255.224<br>
exit<br>
int vlan 34<br>
description vinterface vlan administracion<br>
ip address 192.168.14.65 255.255.255.224<br>
exit<br>
int vlan 44<br>
description vinterface vlan servidores<br>
ip address 192.168.14.97 255.255.255.224<br>
exit<br>
exit<br>
show ip route <br>
copy running-config startup-config <br>

### Switch 0 (S0)
> enable  <br>
conf t <br>
vtp domain g13 <br>
vtp password g13  <br>
vtp mode client <br>
exit <br>
show vtp status <br>
show vl <br>
conf t <br>
interface fa0/1 <br>
switchport mode trunk <br>
exit <br>
interface fa0/2 <br> 
switchport mode trunk <br>
exit <br>
interface fa0/3 <br>
switchport mode trunk <br>
exit <br>
interface fa0/4 <br>
switchport mode trunk <br>
exit <br>
interface fa0/5 <br>
switchport mode trunk <br>
exit <br>
interface fa0/6 <br>
switchport mode trunk <br>
exit <br>
interface fa0/7 <br> 
switchport mode trunk <br>
exit <br>
### LACP
interface range fa0/2 - 3 <br>
channel-protocol lacp <br>
channel-group 1 mode active <br>
exit <br>
interface range fa0/4 - 5 <br>
channel-protocol lacp <br>
channel-group 2 mode active <br>
exit <br>
interface range fa0/6 - 7 <br>
channel-protocol lacp <br>
channel-group 4 mode active <br>
exit <br>
### PAgP
interface range fa0/2 - 3 <br>
channel-protocol pagp <br>
channel-group 1 mode auto <br>
exit <br>
interface range fa0/4 - 5 <br>
channel-protocol pagp <br>
channel-group 2 mode auto <br>
exit <br>
interface range fa0/6 - 7 <br>
channel-protocol pagp <br>
channel-group 4 mode auto <br>
exit <br>
do show eth<br>
spanning-tree mode pvst<br>
exit <br>
copy running-config startup-config  <br>


### Switch 1 (S1)
> enable <br>
conf t <br>
vtp domain g13 <br>
vtp password g13 <br>
vtp mode client <br>
exit <br>
show vtp status <br>
show vl <br>
show vl <br>
conf t <br>
interface fa0/1 <br>
switchport mode trunk <br>
exit <br>
interface fa0/2 <br>
switchport mode trunk <br>
exit <br>
interface fa0/3 <br>
switchport mode trunk <br>
exit <br>
interface fa0/4 <br>
switchport mode trunk <br>
exit <br>
interface fa0/5 <br>
switchport mode trunk <br>
exit <br>
### LACP
interface range fa0/2 - 3 <br>
channel-protocol lacp <br>
channel-group 1 mode active <br>
exit <br>
### PAgP
interface range fa0/2 - 3 <br>
channel-protocol pagp <br>
channel-group 1 mode auto <br>
exit <br>

do show eth<br>
spanning-tree mode pvst<br>
exit <br>
copy running-config startup-config <br>

### Switch 2 (S2)
> enable  <br>
conf t <br>
vtp domain g13 <br>
vtp password g13 <br>
vtp mode client <br>
exit <br>
show vl <br>
conf t <br>
interface fa0/1 <br>
switchport mode trunk <br>
exit <br>
interface fa0/2  <br>
switchport mode trunk <br>
exit <br>
interface fa0/3 <br> 
switchport mode trunk <br>
exit <br>
interface fa0/4<br> 
switchport mode trunk <br>
exit <br>
interface fa0/5 <br>
switchport mode access <br>
switchport access vlan 14 <br>
exit <br>
interface fa0/6 <br>
switchport mode access <br>
switchport acess vlan 14 <br>
exit <br>
interface fa0/7 <br>
switchport mode access <br>
switchport acess vlan 24 <br>
exit <br>
interface fa0/8 <br>
switchport mode access <br>
switchport acess vlan 24 <br>
exit <br>
exit <br>
show vtp status <br>
show vl <br>
conf t 
### LACP
interface range fa0/1 - 2 <br>
channel-protocol lacp <br>
channel-group 2 mode active <br>
exit <br>
interface range fa0/3 - 4 <br>
channel-protocol lacp <br>
channel-group 3 mode active <br>
exit <br>
### PAgP
interface range fa0/1 - 2 <br>
channel-protocol pagp <br>
channel-group 2 mode auto <br>
exit <br>
interface range fa0/3 - 4 <br>
channel-protocol pagp <br>
channel-group 3 mode auto <br>
exit <br>
do show eth<br>
spanning-tree mode pvst<br>
exit <br>
copy running-config startup-config  <br>

### Switch 3(S3)
> enable  <br>
conf t <br>
vtp domain g13 <br>
vtp password g13 <br>
vtp mode client <br>
exit <br>
show vl <br>
conf t <br>
interface fa0/1 <br>
switchport mode trunk <br>
exit <br> 
interface fa0/2<br> 
switchport mode trunk <br>
exit <br>
interface fa0/3 <br>
switchport mode trunk <br>
exit <br>
interface fa0/4 <br>
switchport mode trunk <br>
exit <br>
interface fa0/5 <br>
switchport mode access <br>
switchport access vlan 14 <br>
exit <br>
interface fa0/6 <br>
switchport mode access <br>
switchport acess vlan 24 <br>
exit <br>
### LACP
conf t <br>
interface range fa0/1 - 2 <br>
channel-protocol lacp <br>
channel-group 4 mode active <br>
exit <br>
interface range fa0/3 - 4 <br>
channel-protocol lacp <br>
channel-group 3 mode active <br>
exit <br>
### PAgP
conf t <br>
interface range fa0/1 - 2 <br>
channel-protocol pagp <br>
channel-group 4 mode auto <br>
exit <br>
interface range fa0/3 - 4 <br>
channel-protocol pagp <br>
channel-group 3 mode auto <br>
exit <br>
<br>
<br>
do show eth<br>
spanning-tree mode pvst<br>
exit <br>
copy running-config startup-config  <br>

### Switch 4 (S4)
> enable  <br>
conf t <br>
vtp domain g13 <br>
vtp password g13 <br>
vtp mode client <br>
exit <br>
show vl <br>
conf t <br>
interface fa0/1 <br>
switchport mode trunk <br>
exit <br>
interface fa0/2 <br>
switchport mode access <br>
switchport acess vlan 34 <br>
exit <br>
interface fa0/3 <br>
switchport mode access <br>
switchport acess vlan 44 <br>
exit <br>
exit <br>
copy running-config startup-config <br>
### Switch 5 (S5)
> enable <br>
conf t <br>
vtp domain g13 <br>
vtp password g13 <br>
vtp mode client <br>
exit <br>
show vl <br>
conf t <br>
interface fa0/1 <br>
switchport mode trunk <br>
exit <br>
interface fa0/2 <br>
switchport mode access <br>
switchport acess vlan 34 <br>
exit <br>
interface fa0/3 <br>
switchport mode access <br>
switchport acess vlan 44 <br>
exit <br>
exit <br>
copy running-config startup-config <br>




----- R1
enable
configure terminal
interface range fastEthernet 0/1-2
switchport trunk native vlan 99
exit

exit
copy running-config startup-config 


----- SWITCH 0
enable
configure terminal

interface range fastEthernet 0/1-7
switchport mode trunk
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 1
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 2
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 3
switchport trunk native vlan 99
no shutdown 
exit

exit 
copy running-config startup-config 



----- SWITCH 1
enable
configure terminal

interface range fastEthernet 0/1-5
switchport mode trunk
switchport trunk native vlan 99
exit


interface port-channel 1
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 




----- SWITCH 2
enable
configure terminal
interface range fastEthernet 0/1-8
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

interface port-channel 1
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 2
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 3
switchport trunk native vlan 99
no shutdown 
exit

exit
copy running-config startup-config 




----- SWITCH 3
enable
configure terminal
interface range fastEthernet 0/1-6
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 




----- SWITCH 4
enable
configure terminal
interface range fastEthernet 0/1-3
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 



----- SWITCH 5
enable
configure terminal
interface range fastEthernet 0/1-3
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 


