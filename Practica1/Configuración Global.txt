
----- VLAN
Crear:
	enable
	config t
	vlan #x
	name xxxx
	end 
Mostrar:
	show vlan

----- PORTS
Access Mode:
	enable
	config t
	interface fa0/x
	switchport mode access
	switchport access vlan #x
Trunk Mode:
	enable
	config t
	interface fa0/#
	switchport mode trunk
	end

-GUARDAR 
copy running-config startup-config 



----- R1
enable
configure terminal
interface range fastEthernet 0/1-2
switchport trunk native vlan 99
exit

exit
copy running-config startup-config 


----- SWITCH 0
enable
configure terminal

interface range fastEthernet 0/1-7
switchport mode trunk
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 1
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 2
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 3
switchport trunk native vlan 99
no shutdown 
exit

exit 
copy running-config startup-config 



----- SWITCH 1
enable
configure terminal

interface range fastEthernet 0/1-5
switchport mode trunk
switchport trunk native vlan 99
exit


interface port-channel 1
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 




----- SWITCH 2
enable
configure terminal
interface range fastEthernet 0/1-8
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

interface port-channel 1
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 2
switchport trunk native vlan 99
no shutdown 
exit

interface port-channel 3
switchport trunk native vlan 99
no shutdown 
exit

exit
copy running-config startup-config 




----- SWITCH 3
enable
configure terminal
interface range fastEthernet 0/1-6
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 




----- SWITCH 4
enable
configure terminal
interface range fastEthernet 0/1-3
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 



----- SWITCH 5
enable
configure terminal
interface range fastEthernet 0/1-3
switchport trunk native vlan 99
no cdp enable
no shutdown 
exit

exit
copy running-config startup-config 


