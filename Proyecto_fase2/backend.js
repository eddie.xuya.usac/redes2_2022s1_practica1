
//Routes
//Servidor
require('dotenv').config()
var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var app = express();
const cors = require('cors');

var mysql      = require('mysql');
dbServer = {
    user: process.env.USER_MYSQL,
    port: process.env.PORT_MYSQL,
    password: process.env.PASSWORD_MYSQL,
    host: process.env.HOST_MYSQL,
    database: process.env.DATABASE_MYSQL
}
var connection = mysql.createConnection(dbServer);
// cors -----------------------------
var corsOptions = { origin: true, optionsSuccessStatus: 200 };
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.use(morgan('dev'));



// RUTAS ------------------------------------------------------------------------
app.get('/',async(req, res) => {
    res.json({ mensaje: 'Api is at /api'})
});

app.get("/devs", async (req, res) => {
    try {
          let sql = `SELECT nombre, img_url, carnet, info FROM Devs;`;
          connection.query(sql, function (err, result) {
              if (err) throw err;
              res.send(result);
            });        
    } catch (error) {
      console.log("Error al conectar a la base de datos => ", error);
      res.json({});
    }
  })

app.get("/imgs", async(req, res) => {
    try {
          let sql = `SELECT nombre, img_url FROM Img`;
          connection.query(sql, function (err, result) {
              if (err) throw err;
              res.send(result);
            });        
    } catch (error) {
      console.log("Error al conectar a la base de datos => ", error);
      res.json({});
    }
  }
  )


var port = process.env.PORT;
app.listen(port, function(){
console.log('Server on port:', port);
});

module.exports = app;


